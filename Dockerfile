# instala o jenkins.

FROM jenkins/jenkins:lts
USER root

RUN mkdir -p /tmp/download && \
 curl -L https://download.docker.com/linux/static/stable/x86_64/docker-18.03.1-ce.tgz | tar -xz -C /tmp/download && \
 rm -rf /tmp/download/docker/dockerd && \
 mv /tmp/download/docker/docker* /usr/local/bin/ && \
 rm -rf /tmp/download && \
 groupadd -g 999 docker && \
 usermod -aG staff,docker jenkins

USER jenkins

# Configurar a rede 
# docker network create skynet

# Criar o volume
# docker volume create jenkins-data

# Executar o volume
# docker container run --name jenkins-automacao --detach --network skynet -u root --volume jenkins-data:/var/jenkins_home --volume /var/run/docker.sock:/var/run/docker.sock --publish 8080:8080 --publish 50000:50000 jenkins/jenkins:lts-jdk11

# Comando para acessar o terminal do container
# docker exec -it <Pegar o id do container com o comando docker ps> bash

# Comando para pegar a chave de segurança
# cat /var/jenkins_home/secrets/initialAdminPassword

# comando para sair de dentro do container
# exit
