# Classe para mapear os elementos da pagina home
class UserPage < SitePrism::Page
  set_url ''
  element :name, '#firstName'
  element :last_name, '#lastName'
  element :btn_proximo, :xpath, 'button[jsname="LgbsSe"]'

  def create_user
    name.set('Wilson')
    last_name.set('Barboza Silva Santos')
  end
end