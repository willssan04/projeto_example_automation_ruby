# encode: UTF-8
# modelo de classe para trabalhar com API
class SoapUiClasseDeAcaoAPIs

    include HTTParty
  
    def request_nomedometodoadequado
      $numeroaleatorio = rand(100000000000..999999999999)
      valoresparaapi = ["123456"]
      $geracao_numeroaleatorio = numeroaleatorio.sample
      $request = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:cm='http://ouaf.oracle.com/webservices/cm/??????'><soapenv:Header><wsse:Security soapenv:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'><wsse:UsernameToken><wsse:Username>???????</wsse:Username><wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>??????</wsse:Password></wsse:UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><cm:??????><cm:input><cm:??????>#{$variavelcriada}</cm:???????><cm:???????>#{$variavelcriada}</cm:??????><cm:??????>341</cm:????????><cm:dominio>?????</cm:dominio><cm:fabrica>??????</cm:?????><cm:fechaLabradoInfraccion>??????>"
    end
  
    def post_darequesteacima
      xml_request = Nokogiri::XML($request)
      body = xml_request.to_xml
      username = 'usuario_api'
      password = 'senha_api'
      encoded_login = Base64.strict_encode64("#{username}:#{password}")
  
      $response = HTTParty.post(
        $admin_caminera_url[$env],
        # obrigatorio usar o verify abaixo para acessar as apis do banco sem certificado
        :verify => false,
        body: body,
        headers: { 'Content-Type' => 'text/xml',
          'SOAPAction' => "https://url_soap?wsdl",
          'Authorization' => "Basic #{encoded_login}",
          'Authentication Type' => 'No Authorization',
          'WSS-Password Type' => 'PasswordText'
        }
      )
      $dado_api = $response["Envelope"]["Body"]["dado_retorno_api"]["output"]["dado"]
      $dado_api = $response["Envelope"]["Body"]["dado_retorno_api"]["input"]["dado"]
      p "imprimir_no_teste_o_valor_desejado: #{dado_api}"
      p "imprimir_no_teste_o_valor_desejado: #{$dado_api} "
    end
  
  end