# frozen_string_literal: true

# carregar os arquivos pages do projeto 
Dir[File.join(File.dirname(__FILE__),
    '../pages/*.rb')].sort.each { |file| require file }

# modulos para chamar as classes instanciadas
module ClassHelp

def soapUi
SoapUiNomeClasseAPI.new
end

end
