# Modulo para testes de api, onde informamos as urls de qa ou préprod

module LinkHelp

    $url_api = {
      'qa' => 'URL_DO_SERVIÇO_SOAP_QA',
      'preProd' => 'URL_DO_SERVIÇO_SOAP_PREPROD'
    }
  
end