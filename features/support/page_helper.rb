# frozen_string_literal: true
# carregar os arquivos page.
Dir[File.join(File.dirname(__FILE__),
  '../pages/*.rb')].sort.each { |file| require file }

module PageHelp
  def usepage
    UserPage.new
  end
end

